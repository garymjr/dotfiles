# Change default zim location
export ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim

# Start zim
[[ -s ${ZIM_HOME}/init.zsh ]] && source ${ZIM_HOME}/init.zsh

# Define aliases
alias palette='for i in {0..255}; do echo -e "\e[38;05;${i}m${i}"; done | column -c 180 -s "  "; echo -e "\e[m"'

alias vim=nvim

# Export variables
export EDITOR='nvim'
export VISUAL='nvim'
export GIT_EDITOR='nvim'
export SHELL=/bin/zsh

# Add golang to path
export PATH=$HOME/go/bin:$PATH
if [ -d /usr/local/opt/go/libexec/bin ]; then
  export PATH=/usr/local/opt/go/libexec/bin:$PATH
fi

# Add cargo to path
if [ -d $HOME/.cargo/bin ]; then
  export PATH=$HOME/.cargo/bin:$PATH
fi

# Add local bin to path
if [ -d $HOME/bin ]; then
  export PATH=$HOME/bin:$PATH
fi

# Settings for macos
if [ -d /usr/local/bin ]; then
  export PATH=/usr/local/bin:$PATH
fi

if [ $(uname) = "Darwin" ]; then
  export PATH=/usr/local/share/dotnet:~/.dotnet/tools:$PATH
  export PATH=$HOME/Library/Python/3.7/bin:$PATH
fi

# Check for local zsh config and load it
if [ -s "$HOME/.zshrc.local" ]; then
  . "$HOME/.zshrc.local"
fi

# Rbenv
eval "$(rbenv init -)"

export PREFIX=$HOME/n
export N_PREFIX=$HOME/n
export PATH=$N_PREFIX/bin:$PATH

# function for making tmux a little nicer
function tm() {
  if [[ $1 = "ls" ]]; then
    tmux ls
  elif [[ $1 ]]; then
    tmux new -A -s $1
  else tmux new -A -s $(basename $PWD)
  fi
}

[ -d /usr/local/opt/fzf ] && source /usr/local/opt/fzf/shell/completion.zsh && \
  source /usr/local/opt/fzf/shell/key-bindings.zsh

[ -d /usr/share/fzf ] && source /usr/share/fzf/completion.zsh && \
  source /usr/share/fzf/key-bindings.zsh

export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --glob "!.git/*"'

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
