UNAME := $(shell uname)

init_vim:
	[ -f ${PWD}/.vim/autoload/plug.vim ] || curl -fLo ${PWD}/.vim/autoload/plug.vim --create-dirs \
		    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

deploy_vim:
	[ ! -d ${HOME}/.vim ] || rm -rf ${HOME}/.vim
	ln -fnsv ${PWD}/.vim ${HOME}/.vim
	ln -fnsv ${PWD}/.vimrc ${HOME}/.vimrc
	vim -c ":PlugInstall | :quitall"
		
git:
	ln -fnsv ${PWD}/.gitconfig ${HOME}/.gitconfig
	ln -fnsv ${PWD}/.gitignore_global ${HOME}/.gitignore_global

tmux:
	[ ! -d ${HOME}/.tmux ] || rm -rf ${HOME}/.tmux
	ln -fnsv ${PWD}/.tmux.conf ${HOME}/.tmux.conf
	git clone https://github.com/tmux-plugins/tpm ${HOME}/.tmux/plugins/tpm

zsh:
	ln -fnsv ${PWD}/.zshrc ${HOME}/.zshrc
	ln -fnsv ${PWD}/.zimrc ${HOME}/.zimrc

alacritty:
	[ -d ${HOME}/.config/alacritty ] || mkdir -p ${HOME}/.config/alacritty
ifeq ($(UNAME), Linux)
	ln -fnsv ${PWD}/.config/alacritty/alacritty.yml ${HOME}/.config/alacritty/alacritty.yml
else
	ln -fnsv ${PWD}/.config/alacritty/alacritty_macos.yml ${HOME}/.config/alacritty/alacritty.yml
endif

openbox:
	ln -fnsv ${PWD}/.xinitrc ${HOME}/.xinitrc
	[ ! -d ${HOME}/.themes ] || rm -rf ${HOME}/.themes
	ln -fnsv ${PWD}/.themes ${HOME}/.themes
	[ ! -d ${HOME}/.config/openbox ] || rm -rf ${HOME}/.config/openbox
	ln -fnsv ${PWD}/.config/openbox ${HOME}/.config/openbox

linux_misc:
	[ ! -d ${HOME}/.config/compton ] || rm -rf ${HOME}/.config/compton
	ln -fnsv ${PWD}/.config/compton ${HOME}/.config/compton
	[ ! -d ${HOME}/.config/polybar ] || rm -rf ${HOME}/.config/polybar
	ln -fnsv ${PWD}/.config/polybar ${HOME}/.config/polybar
	[ ! -d ${HOME}/.config/tint2 ] || rm -rf ${HOME}/.config/tint2
	ln -fnsv ${PWD}/.config/tint2 ${HOME}/.config/tint2
	[ ! -d ${HOME}/.config/rofi ] || rm -rf ${HOME}/.config/rofi
	ln -fnsv ${PWD}/.config/rofi ${HOME}/.config/rofi

local_bin:
	[ ! -d ${HOME}/bin ] || rm -rf ${HOME}/bin
	ln -fnsv ${PWD}/bin ${HOME}/bin

init_kakoune:
	[ ! -d ${PWD}/.config/kak/plugins ] || rm -rf ${PWD}/.config/kak/plugins
	git clone https://github.com/andreyorst/plug.kak.git ${PWD}/.config/kak/plugins/plug.kak
	ln -fnsv ${PWD}/.config/kak/plugins/kakoune-snippets/bin/emmet-call ${PWD}/.config/kak/bin
	ln -fnsv ${PWD}/.config/kak/plugins/kakoune-snippets/bin/snippet ${PWD}/.config/kak/bin

deploy_kakoune:
	[ ! -d ${HOME}/.config/kak ] || rm -rf ${HOME}/.config/kak
	ln -fnsv ${PWD}/.config/kak ${HOME}/.config/kak

kakoune: init_kakoune deploy_kakoune
vim: init_vim deploy_vim

.PHONY: vim kakoune
