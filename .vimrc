" ==================
" Section: Setup {{{
" ==================

unlet! skip_defaults_vim
silent! source $VIMRUNTIME/defaults.vim

let mapleader = ' '
let maplocalleader = ' '

call plug#begin('~/.vim/plugged')

Plug 'sbdchd/neoformat'
Plug 'tpope/vim-commentary'
Plug 'markonm/traces.vim'
Plug 'wellle/targets.vim'
Plug 'sheerun/vim-polyglot'
Plug 'joshdick/onedark.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mattn/emmet-vim'
Plug 'SirVer/ultisnips'
Plug 'ntpeters/vim-better-whitespace'
Plug 'sbdchd/neoformat'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}

call plug#end()
" }}}

" =====================
" Section: Settings {{{
" =====================

set autoindent
set autoread
set background=light
set backspace=indent,eol,start
set belloff=all
set breakindent
set breakindentopt=shift:2
set clipboard^=unnamed,unnamedplus
set complete-=i
set complete-=t
set completeopt=menu,menuone,noinsert,noselect
set expandtab
set fillchars=vert:┃
set fillchars+=fold:·
set foldmethod=indent
set foldlevelstart=99
set grepprg=rg\ --color=never
set hidden
set history=10000
set ignorecase
set incsearch
set laststatus=2
set linebreak
set mouse=a
set nobackup
set nohlsearch
set nomodeline
set noshowcmd
set noshowmode
set noswapfile
set path=.
set path+=**
set re=1
set ruler
set shiftwidth=2

set shortmess+=I " no splash screen
set shortmess+=W " don't echo written when writing file
set shortmess+=a " use abbreviations in messages
set shortmess+=c " no completion messages

if has('linebreak')
  let &showbreak='⤷ '
endif

set smartcase
set splitbelow
set splitright
set tabstop=2
set termguicolors
set timeoutlen=1000 ttimeoutlen=0
set ttyfast
set undodir=~/.vim/undo
set undodir+=.
set undofile

set updatetime=2000 " for checktime / CursorHold

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
set wildignore+=*/node_modules/*
set wildmenu
set wildmode=list:longest,list:full
" }}}

" ============================
" Section: Plugin Settings {{{
" ============================

let g:neoformat_javascript_prettier = {
      \ 'exe': 'prettier-eslint',
      \ 'args': ['--stdin', '--stdin-filepath', '%:p'],
      \ 'stdin': 1,
      \ }
let g:neoformat_enabled_javascript = ['prettier']

let g:neoformat_vue_prettier = {
      \ 'exe': 'prettier-eslint',
      \ 'args': ['--stdin', '--stdin-filepath', '%:p'],
      \ 'stdin': 1,
      \ }
let g:neoformat_enabled_vue = ['prettier']

let g:neoformat_python_black = {
      \ 'exe': 'black',
      \ 'args': ['--line-length', '100', '-', '2>/dev/null'],
      \ 'stdin': 1,
      \ }
let g:neoformat_enabled_python = ['black']
let g:neoformat_enabled_go = ['goimports']

let g:go_fmt_autosave = 0
" }}}

" =======================
" Section: Appearance {{{
" =======================

augroup ColorScheme
  au!
  au VimEnter * call SetupDefaultItalics()
  au ColorScheme * call SetupDefaultItalics()
augroup END

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

colorscheme onedark
" }}}

" ======================
" Section: Functions {{{
" ======================

func! s:get_highlight_group(group) abort
  let output = execute('hi ' . a:group)
  let list = split(output, '\s\+')
  let dict = {}
  for item in list
    if match(item, '=') > 0
      let split_item = split(item, '=')
      let dict[split_item[0]] = split_item[1]
    endif
  endfor
  return dict
endfunc

func! HighlightWithItalics(group, type) abort
  let hlgroup = s:get_highlight_group(a:group)
  let guifg = has_key(hlgroup, 'guifg') ? hlgroup.guifg : 'NONE'
  execute('hi ' . a:type . ' gui=italic guifg=' . guifg)
endfunc

func! ToggleNumbers() abort
  if &relativenumber
    set nonumber
    set norelativenumber
  elseif &number
    set relativenumber
  else
    set number
  endif
endfunc

func! SetupDefaultItalics() abort
  hi Comment   gui=italic cterm=italic
  hi xmlAttrib gui=italic cterm=italic
endfunc
" }}}

" ====================
" Section: Autocmd {{{
" ====================

augroup DotVim
  au!

  " Update files every two seconds
  au CursorHold,FocusGained,WinEnter * checktime

  " Always put quickfix at the bottom
  au FileType qf wincmd J

  " Fix for css keywords
  au FileType css setlocal iskeyword+=-

  " Autoformat on save
  au BufWritePre *.go Neoformat
augroup END

" Keep Vue files syntax synced
augroup Vue
  au!
  au BufEnter,FocusGained,BufWrite,CursorHold *.vue syntax sync fromstart
augroup END
" }}}

" =====================
" Section: Mappings {{{
" =====================

" Use <esc> to exit insert mode in terminal
tnoremap <esc> <c-\><c-n>

" maintain selection when shifting selection
vmap < <gv
vmap > >gv

" make backspace useful in normal mode
nnoremap <backspace> <c-^>

" make Y act like D
nnoremap Y y$

nnoremap <silent> - :e <c-r>=expand('%:p:h') . '/'<cr><cr>

" easier emmet expansion
inoremap <silent> <c-e> <c-r>=emmet#expandAbbr(0, "")<cr>

nnoremap <silent> / :BLines<cr>

" <leader>
nnoremap <silent> <leader><leader> :GFiles<cr>
nnoremap <silent> <leader><tab> :Buffers<cr>
nnoremap <silent> <leader>c <c-w>c
nnoremap <silent> <leader>e :NERDTreeToggle<cr>
nnoremap <silent> <leader>f :GFiles<cr>
nnoremap <silent> <leader>h :Helptags<cr>
nnoremap <silent> <leader>n :call ToggleNumbers()<cr>
nnoremap <silent> <leader>s :Lines<cr>

nnoremap <silent> <leader>wh <c-w>h
nnoremap <silent> <leader>wj <c-w>j
nnoremap <silent> <leader>wk <c-w>k
nnoremap <silent> <leader>wl <c-w>l
nnoremap <silent> <leader>ws :sp<cr>
nnoremap <silent> <leader>wv :vsp<cr>

nnoremap <silent> <leader>r :Neoformat<cr>
" }}}
