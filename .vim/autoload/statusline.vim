function! statusline#fileprefix() abort
  let l:basename=expand('%:h')
  if l:basename ==# '' || l:basename ==# '.'
    return ''
  else
    " Make sure we show $HOME as ~.
    return substitute(l:basename . '/', '\C^' . $HOME, '~', '')
  endif
endfunction

function! statusline#modified() abort
  if &modified
    return ' + '
  else
    return ' '
  endif
endfunction

function! statusline#readonly() abort
  if &readonly || !&modifiable
    return '!'
  else
    return ''
  endif
endfunction

function! statusline#color(num, content) abort
  return '%' . a:num . '*' . a:content . '%*'
endfunction

function! statusline#update() abort
  for nr in range(1, winnr('$'))
    call setwinvar(nr, '&statusline', '%!statusline#init(' . nr . ')')
  endfor
endfunction

function! statusline#init(winnum) abort
  let l:active = a:winnum == winnr()
  let l:bufnum = winbufnr(a:winnum)

  let l:stat = statusline#color(5, ' '.l:bufnum.' ')
  let l:stat .= statusline#color(1, ' ')
  let l:stat .= statusline#color(1, statusline#fileprefix())
  let l:stat .= statusline#color(2, '%t')
  let l:stat .= '%<'

  if l:active
    let l:stat .= statusline#color(3, statusline#modified())
    let l:stat .= statusline#color(4, statusline#readonly())
    let l:stat .= '%='
    let l:stat .= statusline#color(5, ' %l:%c ')
  else
    let l:stat .= '%=%1*'
  endif
  return l:stat
endfunction
